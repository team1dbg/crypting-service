package org.rest.service;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Base64;
import org.crypting.CryptingKeyManager;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


@Service
public class CrypterService {

    private CryptingKeyManager cryptingKeyManager;

    public CrypterService() throws CryptoException, IOException {
        cryptingKeyManager = new CryptingKeyManager();
    }

    public String makePKCS7(byte[] dataToSign) throws Exception {
        Security.addProvider(new BouncyCastleProvider());

        List<X509Certificate> certList = new ArrayList<>(1);
        CMSTypedData msg = new CMSProcessableByteArray(dataToSign);

        certList.add(cryptingKeyManager.getPkCertificate());

        Store certs = new JcaCertStore(certList);

        CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
        ContentSigner sha1Signer = new JcaContentSignerBuilder("SHA1withRSA").setProvider("BC")
                .build(cryptingKeyManager.getPrivateKey());

        gen.addSignerInfoGenerator(
                new JcaSignerInfoGeneratorBuilder(
                        new JcaDigestCalculatorProviderBuilder().setProvider("BC").build())
                        .build(sha1Signer, cryptingKeyManager.getPkCertificate()));

        gen.addCertificates(certs);

        CMSSignedData sigData = gen.generate(msg, false);
        byte[] bb = sigData.getEncoded();
        return Base64.toBase64String(bb);
    }

    public void verifyPKCS7(byte[] sigData) throws Exception {
        CMSSignedData s = new CMSSignedData(sigData);
        Store store = s.getCertificates();
        SignerInformationStore signers = s.getSignerInfos();
        Collection c = signers.getSigners();
        for (Object o : c) {
            SignerInformation signer = (SignerInformation) o;
            Collection certCollection = store.getMatches(signer.getSID());
            Iterator certIt = certCollection.iterator();
            X509CertificateHolder certHolder = (X509CertificateHolder) certIt.next();
            if (!certHolder.getSerialNumber().equals(cryptingKeyManager.getNsdCertificate().getSerialNumber()))
                throw new Exception("Error verifying signature: certificate in signature (SN=" + certHolder.getSerialNumber()
                        + ") doesn't fit certificate (CN="
                        + cryptingKeyManager.getNsdCertificate().getSerialNumber() + ") ");

            if (!signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC")
                    .build(cryptingKeyManager.getNsdCertificate()))) {
                throw new Exception("Signature is not verified");
            }
        }
    }


}