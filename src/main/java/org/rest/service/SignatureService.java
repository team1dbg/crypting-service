package org.rest.service;

import org.bouncycastle.util.encoders.Base64;
import org.rest.dto.SignedStringDTO;
import org.rest.dto.UnsignedStringDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignatureService {

    @Autowired
    CrypterService crypterService;

    public SignedStringDTO signString(UnsignedStringDTO dto) throws Exception {
        String unsignedString = new String(Base64.decode(dto.getUnsignedStringContent()));

        String signedString = crypterService.makePKCS7(unsignedString.getBytes());

        SignedStringDTO signedStringDTO = new SignedStringDTO();
        signedStringDTO.setUserId(dto.getUserId());
        signedStringDTO.setSignedStringContent(Base64.toBase64String(signedString.getBytes()));
        return signedStringDTO;
    }

    public UnsignedStringDTO verifyString(SignedStringDTO dto) {
        UnsignedStringDTO unsignedStringDTO = new UnsignedStringDTO();
        unsignedStringDTO.setUserId(dto.getUserId());
        try {
            crypterService.verifyPKCS7(dto.getSignedStringContent().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            unsignedStringDTO.setUnsignedStringContent(e.getMessage());
            return unsignedStringDTO;
        }
        unsignedStringDTO.setUnsignedStringContent("verified");
        return unsignedStringDTO;
    }

}