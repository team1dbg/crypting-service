package org.rest.service;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduledTasks {
    final static Logger log = Logger.getLogger(ScheduledTasks.class);

    @Scheduled(fixedRateString = "${scheduling.fixed.rate}")
    public static void processIncomingFiles() {

        System.out.println("11");
        //log.debug("11");

    }

}
