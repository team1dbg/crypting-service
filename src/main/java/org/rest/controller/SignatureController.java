package org.rest.controller;

import org.rest.dto.SignedStringDTO;
import org.rest.dto.UnsignedStringDTO;
import org.rest.service.SignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SignatureController {

    @Autowired
    SignatureService signatureServiceNrd;

    //TODO - add exceptions filter to not throw all error messages to response
    @PostMapping("/string/sign")
    public SignedStringDTO signString(@RequestBody UnsignedStringDTO dto) throws Exception {
        return signatureServiceNrd.signString(dto);
    }

    @PostMapping("/string/verify")
    public UnsignedStringDTO verifyString(@RequestBody SignedStringDTO dto) {
        return signatureServiceNrd.verifyString(dto);
    }

}