package org.rest.dto;

public class UnsignedStringDTO {
    private long userId;
    private String unsignedStringContent;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUnsignedStringContent() {
        return unsignedStringContent;
    }

    public void setUnsignedStringContent(String unsignedStringContent) {
        this.unsignedStringContent = unsignedStringContent;
    }

    @Override
    public String toString() {
        return "{" +
                "\"userId\":" + userId +
                ", \"unsignedStringContent\":\"" + unsignedStringContent + '\"' +
                '}';
    }
}
