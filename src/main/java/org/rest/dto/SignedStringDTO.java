package org.rest.dto;

public class SignedStringDTO {
    private long userId;
    private String signedStringContent;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getSignedStringContent() {
        return signedStringContent;
    }

    public void setSignedStringContent(String signedStringContent) {
        this.signedStringContent = signedStringContent;
    }

    @Override
    public String toString() {
        return "{" +
                "\"userId\":" + userId +
                ", \"signedStringContent\":\"" + signedStringContent + '\"' +
                '}';
    }
}
