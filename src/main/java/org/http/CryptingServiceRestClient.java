package org.http;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.bouncycastle.util.encoders.Base64;
import org.rest.dto.SignedStringDTO;
import org.rest.dto.UnsignedStringDTO;

import java.io.IOException;

public class CryptingServiceRestClient extends RestClient {
    private String signUrl;
    private String verifyUrl;

    public CryptingServiceRestClient() throws IOException {
    }

    public SignedStringDTO signString(UnsignedStringDTO requestDto) throws IOException {
        HttpPost post = new HttpPost(getSignUrl());
        post.setHeader("Content-Type", "application/json");

        requestDto.setUnsignedStringContent(Base64.toBase64String(requestDto.getUnsignedStringContent().getBytes()));
        post.setEntity(new StringEntity(requestDto.toString()));

        CloseableHttpResponse response = httpClient.execute(post);
        SignedStringDTO responseDto = objectMapper.readValue(response.getEntity().getContent(),
                SignedStringDTO.class);
        response.close();

        return responseDto;
    }

    private String getSignUrl() {
        if(signUrl == null) {
            signUrl = "http://" +
                    properties.getProperty("crypting.server.host") + ":" +
                    properties.getProperty("server.port") +
                    properties.getProperty("crypting.server.url.path.sign");
        }
        return signUrl;
    }

    public UnsignedStringDTO verifyString(SignedStringDTO requestDto) throws IOException {
        HttpPost post = new HttpPost(getVerifyUrl());
        post.setHeader("Content-Type", "application/json");
        post.setEntity(new StringEntity(requestDto.toString().replace("\r\n", "\\r\\n")));

        CloseableHttpResponse response = httpClient.execute(post);
        UnsignedStringDTO responseDto = objectMapper.readValue(response.getEntity().getContent(),
                UnsignedStringDTO.class);
        response.close();

        return responseDto;
    }

    private String getVerifyUrl() {
        if(verifyUrl == null) {
            verifyUrl = "http://" +
                    properties.getProperty("crypting.server.host") + ":" +
                    properties.getProperty("server.port") +
                    properties.getProperty("crypting.server.url.path.verify");
        }
        return verifyUrl;
    }
}
