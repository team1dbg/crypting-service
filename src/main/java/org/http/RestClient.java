package org.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class RestClient {
    //PoolingHttpClientConnectionManager  manages a pool of client connections and able to service connection requests
    //from multiple execution threads - https://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html
    PoolingHttpClientConnectionManager connectionManager;
    CloseableHttpClient httpClient;
    ObjectMapper objectMapper;
    Properties properties;

    public RestClient() throws IOException {
        init();
    }

    public void init() throws IOException {
        initProperties();
        connectionManager = new PoolingHttpClientConnectionManager();
        //increase maximum total number of connections in pool to 200
        connectionManager.setMaxTotal(200);
        //increase default maximum connections per route(URL, port) to 20
        connectionManager.setDefaultMaxPerRoute(20);

        httpClient = HttpClients.custom()
                .setConnectionManager(connectionManager)
                .build();

        objectMapper = new ObjectMapper();
    }

    private void initProperties() throws IOException {
        properties = new Properties();
        InputStream in = null;
        try {
            in = getClass().getClassLoader().getResourceAsStream("application.properties");
            if (in != null) properties.load(in);
            else throw new IOException("Init file properties not found");
        } finally {
            if (in != null) in.close();
        }
    }
}
