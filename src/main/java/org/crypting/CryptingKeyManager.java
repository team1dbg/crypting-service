package org.crypting;

import org.bouncycastle.crypto.CryptoException;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.util.Properties;


public class CryptingKeyManager {

    private Properties properties;
    private RSAPrivateCrtKey privateKey;
    private X509Certificate pkCertificate;
    private X509Certificate nsdCertificate;


    public CryptingKeyManager() throws IOException, CryptoException {
        initProperties();
        init(properties.getProperty("keystore.path"),
                properties.getProperty("keystore.password"),
                properties.getProperty("privateKey.alias"),
                properties.getProperty("serverCert.alias"));
    }

    private void init(String keyStorePath,
                      String password,
                      String privateKeyAlias,
                      String nsdCertAlias) throws CryptoException {
        if (privateKeyAlias == null)
            throw new CryptoException("Private key alias not specified");
        KeyStore keyStore;
        try {
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream in = new ClassPathResource(keyStorePath).getInputStream();
            keyStore.load(in, password.toCharArray());

            pkCertificate = (X509Certificate) keyStore.getCertificate(privateKeyAlias);
            privateKey = (RSAPrivateCrtKey) keyStore.getKey(privateKeyAlias, password.toCharArray());
            nsdCertificate = (X509Certificate) keyStore.getCertificate(nsdCertAlias);
        } catch (Exception e) {
            throw new CryptoException("KeyStore init error", e);
        }
    }

    private void initProperties() throws IOException {
        properties = new Properties();
        InputStream in = null;
        try {
            in = getClass().getClassLoader().getResourceAsStream("application.properties");
            if (in != null) properties.load(in);
            else throw new IOException("Init file properties not found");
        } finally {
            if (in != null) in.close();
        }
        String keystorePath = properties.getProperty("keystore.path");
        if (keystorePath == null) throw new IllegalArgumentException("Not specified path to keystore");

        String keystorePassword = properties.getProperty("keystore.password");
        if (keystorePassword == null) throw new IllegalArgumentException("Not specified password to access to keystore");
    }

    public RSAPrivateCrtKey getPrivateKey() {
        return privateKey;
    }

    public X509Certificate getPkCertificate() {
        return pkCertificate;
    }

    public X509Certificate getNsdCertificate() {
        return nsdCertificate;
    }
}
