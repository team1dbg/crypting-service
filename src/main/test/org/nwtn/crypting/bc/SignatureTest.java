package org.nwtn.crypting.bc;

import org.apache.log4j.Logger;
import org.http.CryptingServiceRestClient;
import org.junit.Ignore;
import org.junit.Test;
import org.rest.dto.SignedStringDTO;
import org.rest.dto.UnsignedStringDTO;

//TODO - add @Before and @After methods for creating files on the beginning of tests and removing after completion
public class SignatureTest {
    final static Logger log = Logger.getLogger(SignatureTest.class);

    //ignored by default because it requires running crypting service
    @Ignore
    @Test
    public void testSignString() throws Exception {
        CryptingServiceRestClient restClient = new CryptingServiceRestClient();

        UnsignedStringDTO requestDto = new UnsignedStringDTO();
        requestDto.setUnsignedStringContent("hello world");
        SignedStringDTO signedStringDTO = restClient.signString(requestDto);
        signedStringDTO.setSignedStringContent(signedStringDTO.getSignedStringContent()
                .replace("\n", "\\r\\n"));
        log.debug("signedStringDTO = " + signedStringDTO);
    }
}
